package main

import (
	"encoding/json"
	"fmt"
	"log"

	"github.com/btcsuite/btcd/rpcclient"
)

func print(val interface{}) {
	data, _ := json.MarshalIndent(val, "", "  ")
	fmt.Println(string(data))
}

func main() {
	client, err := rpcclient.New(&rpcclient.ConnConfig{

		DisableTLS:   true,
		HTTPPostMode: true,
	}, nil)
	if err != nil {
		log.Fatalln(err)
		return
	}

	hash, err := client.GetBlockHash(42886)
	if err != nil {
		log.Fatalln(err)
		return
	}
	print(hash.String())
	// block, err := client.GetBlock(hash)
	// if err != nil {
	// 	log.Fatalln(err)
	// 	return
	// }
	// print(block)
	// txHash := block.Block.Transactions[0].TxHash()
	// tx, err := client.GetTxDetails(&txHash, false)
	// if err != nil {
	// 	log.Fatalln(err)
	// 	return
	// }
	// print(tx)

	bOps, err := client.GetBlockTxOperations(hash)
	if err != nil {
		log.Fatalln(err)
		return
	}
	print(bOps)
}
